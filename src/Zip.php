<?php
declare(strict_types=1);

namespace Zlf\Unit;
use Exception;
use ZipArchive;

class Zip
{
    /**
     * 压缩文件或目录为zip
     * @param string $dir 要备份的文件夹
     * @param string|null $filename 备份的文件名
     */
    public static function pack(string $dir, ?string $filename = null, $exclude = []): array
    {
        if (is_dir($dir)) {
            if (is_null($filename)) {
                $filename = $dir . '.zip';
            }
            $zip_resource = new ZipArchive();
            try {
                $zip_resource->open($filename, ZipArchive::CREATE);
                $dir_name = pathInfo($dir)['basename'];
                if (is_file($dir)) {
                    $zip_resource->addFile($dir);
                } else {
                    $zip_resource->addEmptyDir($dir_name);
                    self::addZipFile($zip_resource, $dir, $dir_name, $exclude);
                }
                $zip_resource->close();
                return ['state' => true, 'msg' => "目录打包成功", "file" => $filename];
            } catch (Exception $exception) {
                $zip_resource->close();
                return ['state' => false, 'msg' => "目录打包失败", 'error' => $exception->getMessage()];
            }
        }
        return ['state' => false, 'msg' => "{$dir}不是有效的目录"];
    }

    /**
     * 递归将文件夹内的文件添加到对象
     * @param ZipArchive $ZipResource zip对象
     * @param string $path 要添加的文件夹路径
     * @param string|null $dirname zip资源的本地路径
     * @param array $exclude 排除的把内容
     */
    private static function addZipFile(ZipArchive &$ZipResource, string $path, ?string $dirname = null, array $exclude = []): void
    {
        $handle = opendir($path);
        while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..') {
                $filepath = "$path/$f";
                $localpath = trim("$dirname/$f", '/'); //
                if (in_array($filepath, $exclude)) {
                    continue;
                }
                if (is_file($filepath)) {
                    $ZipResource->addFile($filepath, $localpath);
                } else {
                    $ZipResource->addEmptyDir($localpath);
                    self::addZipFile($ZipResource, $filepath, $localpath, $exclude);
                }
            }
        }
    }
}