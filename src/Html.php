<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Html
{


    /**
     * 从Html提取文字摘要
     * @param string $html
     * @param int $limit 截取文字长度
     * @return string
     */
    public static function extractSummary(string $html, int $limit): string
    {
        $text = self::toText($html);
        if (mb_strlen($text) <= $limit) {
            return $text;
        }
        return mb_substr($text, 0, $limit) . '...';
    }


    /**
     * Html转文本
     * @param string $html
     * @param int|null $limit
     * @param int $start
     * @return string
     */
    public static function toText(string $html, ?int $limit = null, int $start = 0): string
    {
        if (strlen($html) > 0) {
            $text = strip_tags($html);
            $text = str_replace(['&nbsp;', '&ldquo;', '&rdquo;', '&mdash;', '&middot;', '&bull;', '&lsquo;'], ['', '', '', '', '', '', ''], $text);
            return $limit ? mb_substr($text, $start, $limit) : $text;
        }
        return '';
    }
}