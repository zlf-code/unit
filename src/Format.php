<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 单位转换工具
 */
class Format
{
    /**
     * 文件容量转换
     * php-赵 2019/2/13 13:29
     */
    public static function size(int $filesize, int $round = 2): string
    {
        if ($filesize < 1024) {
            return $filesize . 'b';
        } else if ($filesize < 1048576) {//kb
            return round($filesize / 1024, $round) . 'kb';
        } else if ($filesize < 1073741824) {//mb
            return round($filesize / 1048576, $round) . 'mb';
        }
        return round($filesize / 1073741824, $round) . 'gb';
    }
}