<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 日期时间工具
 */
class Timeformat
{

    /**
     * 秒转时分秒
     * @param $seconds
     * @return string
     */
    public static function convertSeconds($seconds): string
    {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds % 3600) / 60);
        $remainingSeconds = $seconds % 60;
        $result = "";
        if ($hours > 0) $result .= $hours . "小时";
        if ($minutes > 0) $result .= $minutes . "分钟";
        if ($remainingSeconds > 0) $result .= $remainingSeconds . "秒";
        if (empty($result)) $result = "0秒";
        return $result;
    }


    /**
     * 转换时间显示
     * @param int $timestamp 时间戳
     * @param string $format 转换格式
     * @param bool $convert 是否需要转换
     * @return string
     */
    public static function show(int $timestamp, string $format = 'Y-m-d H:i:s', bool $convert = true): string
    {
        $thistime = time();
        if ($convert === false) {
            return date($format, $timestamp);
        }
        $second = $thistime - $timestamp;
        if ($second < 10) {
            return '刚刚';
        } elseif ($second < 60) {
            return "{$second}秒前";
        } elseif ($second < 3600) {
            return floor($second / 60) . "分钟前";
        } elseif ($second < 86400) {
            if (date("Y-m-d", $thistime) === date("Y-m-d", $timestamp)) {//今天
                return "今天" . date("m:d:s", $timestamp);
            }
            return "昨天" . date("m:d:s", $timestamp);
        }
        return date($format, $timestamp);
    }
}