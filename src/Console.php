<?php
declare(strict_types=1);

namespace Zlf\Unit;

class Console
{
    /**
     * 终端输出.绿色
     * @param string $message
     * @param string $type
     * @return void
     */
    public static function Success(string $message, string $type = ''): void
    {
        if ($type) {
            echo "\033[32m[{$type}]\033[0m {$message}" . PHP_EOL;
        } else {
            echo "\033[32m{$message}\033[0m" . PHP_EOL;
        }
    }

    /**
     * 终端输出.红色
     * @param string $message
     * @param string $type
     * @return void
     */
    public static function Error(string $message, string $type = ''): void
    {
        if ($type) {
            echo "\033[0;31m[{$type}]\033[0m {$message}" . PHP_EOL;
        } else {
            echo "\033[0;31m[{$message}]\033[0m" . PHP_EOL;
        }
    }

    /**
     * 终端输出.黄色
     * @param string $message
     * @param string $type
     * @return void
     */
    public static function Warning(string $message, string $type = ''): void
    {
        if ($type) {
            echo "\033[0;33m[{$type}]\033[0m {$message}" . PHP_EOL;
        } else {
            echo "\033[0;33m[{$message}]\033[0m" . PHP_EOL;
        }
    }

    /**
     * 终端输出.白色
     * @param string $message
     * @param string $type
     * @return void
     */
    public static function Info(string $message, string $type = ''): void
    {
        if ($type) {
            echo "\033[0;37m[{$type}]\033[0m {$message}" . PHP_EOL;
        } else {
            echo "\033[0;37m[{$message}]\033[0m" . PHP_EOL;
        }
    }
}