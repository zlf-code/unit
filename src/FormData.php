<?php
declare(strict_types=1);

namespace Zlf\Unit;

/**
 * 表单数据助手
 * 处理的数据值为value  名称为label   下级为children
 */
class FormData
{
    /**
     * 获取完整的值
     * @param array $data
     * @param string $value
     * @return array
     */
    public static function getFullValue(array $data, string $value): array
    {
        $data = self::unpack($data);
        $maxIndex = count($data) - 1;
        $values = [$value];
        $while = 0;
        while ($value) {
            foreach ($data as $index => $item) {
                if ($item['sign'] === $value) {
                    $value = $item['pid'];
                    if ($value) {
                        $values[] = $value;
                    }
                    break;
                }
                if ($maxIndex === $index) {
                    if ($while === 0) {
                        $values = [];
                    }
                    $value = '';
                }
            }
            $while++;
        }
        return array_reverse($values);
    }


    /**
     * 多维数组数据解包，下级键名必须是children
     * @param array $data
     * @param array $unpack
     * @return array
     */
    public static function unpack(array $data, array $unpack = []): array
    {
        if (Is::list($data)) {//是列表
            foreach ($data as $row) {
                if (isset($row['children']) && count($row['children']) > 0) {
                    $children = $row['children'];
                    unset($row['children']);
                    $unpack[] = $row;
                    $unpack = self::unpack($children, $unpack);
                } else {
                    unset($row['children']);
                    $unpack[] = $row;
                }
            }
        } elseif (isset($data['children']) && count($data['children']) > 0) {
            $children = $data['children'];
            unset($data['children']);
            $unpack[] = $data;
            $unpack = self::unpack($children, $unpack);
        }
        return $unpack;
    }
}